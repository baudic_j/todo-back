const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type Todo {
    id: Int,
    title: String,
    description: String
  },
  #4 Define the query type that must respond to 'posts' query
  type Query {
    todos: [Todo],
    go: String
  },
  #5 Define a mutation to add new posts with two required fields
  type Mutation {
    addTodo(title: String!, description: String!): Todo,
  },
  type Subscription {
    newTodo: Todo
  }
`;

module.exports = typeDefs;