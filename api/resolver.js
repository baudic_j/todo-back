const todoAccess = require('./dataAccess/todoAccess')
const { PubSub } = require('apollo-server-express');

const pubsub = new PubSub();

const TOPIC = 'infoTopic';

const infos = ['info1', 'info2', 'info3', 'done']

const publish = () => {
  setTimeout( () =>
  infos.forEach(newTodo => pubsub.publish(TOPIC, { newTodo })), 1000)
}
const resolvers = {
  Query: {
    todos: () => todoAccess.getAll(),
  },

  Mutation: {
    addTodo: async (parent, newTodo) => {
      const { title, description } = newTodo
      await todoAccess.create(title, description)
      pubsub.publish(TOPIC, { newTodo })
      return newTodo
    }
  },

  Subscription: {
    newTodo: {
      subscribe: () => pubsub.asyncIterator([TOPIC]),
    },
  },
};

module.exports = resolvers;