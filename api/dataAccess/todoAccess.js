const connector = require('../dataBase/Connector');

const getAll = async () => {
  try {
    const connexion = await connector.getConnection();
    const [rows, fields] = await connexion.execute(
      'SELECT `id`, `title`, `description` \
      FROM T_TODO'
    )
    console.log('rows = ', rows)
    await connexion.release();
    return rows
  } catch (error) {
    console.log('error = ', error)
    throw error;
  }
}

const create = async (title, description) => {
  const connexion = await connector.getConnection();
  const [rows, fields] = await connexion.execute(
    'INSERT INTO T_TODO \
      (title, description) \
      VALUES(?, ?)'
    , [title, description]
  )
  await connexion.release();
  return rows && rows.affectedRows !== undefined && rows.affectedRows === 1
}

// Routes
module.exports = {
  getAll,
  create,
}