const mysql = require('mysql2/promise');

const dbConnector = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "julien123",
  database: "todo",
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
});

dbConnector.on('acquire', function (connection) {
  console.log('Connection %d acquired', connection.threadId);
});

dbConnector.on('connection', function (connection) {
  connection.query('SET SESSION auto_increment_increment=1')
});

dbConnector.on('release', function (connection) {
  console.log('Connection %d released', connection.threadId);
});

module.exports = {
  getConnection: async function () {
    const newConnection = await dbConnector.getConnection();
    return newConnection;
  },
  closeConnection: async function () {
    dbConnector.end(res => {
      console.log('res = ', res);
    }).catch(err => {
      console.log('err = ', err);
    }) 
  }
}